userscript-livejournal-style-redirector
=======================================

## About

This is a simple [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting a [LiveJournal](https://www.livejournal.com) page -- will instantly `?format=light` to its URL.
