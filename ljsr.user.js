// ==UserScript==
// @name        livejournal style redirector
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-livejournal-style-redirector
// @downloadURL https://bitbucket.org/dsjkvf/userscript-livejournal-style-redirector/raw/master/ljsr.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-livejournal-style-redirector/raw/master/ljsr.user.js
// @include     https://*.livejournal.com/*
// @run-at      document-start
// @version     1.01
// @grant       none
// ==/UserScript==

if (/.html$/.test(window.location) && !window.location.search) {
    var newLocation = window.location.protocol + '//'
                + window.location.host
                + window.location.pathname + '?format=light';
    window.location.replace(newLocation);
}
